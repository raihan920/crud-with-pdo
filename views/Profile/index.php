<?php
if(!isset($_SESSION['message'])){
    session_start();
}
require_once ('../../vendor/autoload.php');
use App\Message\Message;
use App\Utility\Utility;
use App\Profile\Profile;

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset( $_POST['question']) && isset($_POST['questionFor']) ){
    $addQuestion = new Profile();
    $addQuestion->prepareData($_POST);
    $addQuestion->insertQuestion();
}
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['deleteQuestion'])){
    $delQues = new Profile();
    $delQues->deleteQuestion($_POST['deleteQuestion']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../../public/assets/bootstrap-4.0.0-beta-dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../../public/assets/bootstrap-4.0.0-beta-dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="../../public/assets/css/style.css"/>
    <link rel="stylesheet" href="../../public/assets/font-awesome-4.7.0/css/font-awesome.min.css">

    <script src="../../public/assets/js/jquery-3.2.1.min.js"></script>
    <script src="../../public/assets/js/popper.min.js"></script>
    <script src="../../public/assets/bootstrap-4.0.0-beta-dist/js/bootstrap.min.js"></script>
    <script src="../../public/assets/js/script.js"></script>
    <title>
        Profile
    </title>


</head>

<body>
<div class="container">
        <div class="row">
            <!--Alert Start-->

            <div id="alert-message" class="col-md-12">

                <?php
                   /* Message::Message("hello world");*/
                    if((array_key_exists('message',$_SESSION)) && (!empty($_SESSION['message']))){
                        echo Message::Message();
                    }
                ?>
            </div>
            <!--Alert End-->

            <div class="col-md-12">
                <h3>Teachers Evaluation Questions</h3>
            </div>

            <!-- Main Content Starts Here -->
            <div class="col-md-12">
                <div class="row"> <!-- for showing tow div side by side-->
                    <!-- Question Insert Start -->
                    <div class="col-md-5">

                        <div class="col-md-12"><h4><b>Add Questions</b></h4></div>

                        <form action="" method="post" class="col-md-12">

                            <div class="form-group form-inline col-md-12">
                                <label class="col-md-5" style="text-align: right" for="question">Question:*</label>
                                <div class="col-md-7">
                                    <input type="text" id="question" required="required" name="question" class="col-md-12">
                                </div>
                            </div>

                            <div class="form-group form-inline col-md-12">
                                <label class="col-md-5">Question For:*</label>
                                <div class="col-md-7">
                                    <select name="questionFor" class="form-control col-md-12" required>
                                        <option value="">Choose Option</option>
                                        <option value="1">Both Under Graduate & Graduate</option>
                                        <option value="2">Under Graduate Only</option>
                                        <option value="3">Graduate Only</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="col-md-12 text-center">
                                    <button type="button" class="btn btn-danger"><a href="index.php" style="text-decoration: none; color: white;">Cancel</a></button>
                                    <button name="submit" type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Question Insert End -->

                    <!-- View Questions Start -->
                    <div class="col-md-7">
                        <div class="col-md-12"><h4><b>All Available Questions</b></h4></div>
                        <form class="col-md-12" action="" method="post">
                            <table class="col-md-12 table table-striped">
                                <tr class="row">
                                    <td class="col-md-1"><b>Sl.</b></td>
                                    <td class="col-md-7"><b>Question</b></td>
                                    <td class="col-md-2"><b>Program</b></td>
                                    <td class="col-md-2 text-center"><b>Action</b></td>
                                </tr>
                                <?php
                                    $count = 0;
                                    $viewQuestion = new Profile();
                                    $data = $viewQuestion->viewAllQuestion();
                                    foreach ($data as $singleData){
                                        $count++;
                                ?>
                                <tr class="row">
                                    <td class="col-md-1"><?php echo $count;?></td>
                                    <td class="col-md-7"><?php echo $singleData->question; ?></td>
                                    <td class="col-md-2">
                                        <?php
                                        $quesType = $singleData->question_type;
                                        switch ($quesType){
                                            case 1: echo "UG & G";
                                                break;
                                            case 2: echo "UG";
                                                break;
                                            case 3: echo "G";
                                                break;
                                            default: echo "";
                                        }
                                        ?>
                                    </td>
                                    <td class="col-md-2 text-center"><button class="bg-transparent border-0" name="deleteQuestion" type="submit" value="<?php echo $singleData->id?>" onclick="return confirm('Are you sure you want to delete this question?');"><i class="fa fa-trash text-danger"/></button></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </form>
                    </div>
                    <!-- View Questions End -->

                </div>
            </div>
            <!-- Main Content Ends Here -->
        </div>
    </div>

    <script>
        $('#alert-message').delay(2500).fadeOut(1200);
    </script>
</body>

</html>