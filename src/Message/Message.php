<?php
/**
 * Created by PhpStorm.
 * User: raihan
 * Date: 10-Oct-17
 * Time: 10:19 AM
 */
namespace App\Message;
if(!isset($_SESSION)){
    session_start();
}
use App\Utility\Utility;

class Message
{
    public static function Message($message = NULL){

        if(is_null($message)){
            $_message = self::getMessage();
            return $_message;
        }else{
            self::setMessage($message);
        }
    }

    public static function setMessage($message){
        $_SESSION['message']=$message; 
    }

    public static function getMessage(){
        $_message = $_SESSION['message'];
        /*Utility::dd($_message);

        unset($_SESSION['message']); // it should have worked. but not working.

        Utility::dd($_message);*/
        return $_message;
    }
}