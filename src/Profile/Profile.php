<?php
/**
 * Created by PhpStorm.
 * User: raihan
 * Date: 10-Oct-17
 * Time: 10:13 AM
 */
namespace App\Profile;
use App\Utility\Utility;
use App\Message\Message;
use PDO;

class Profile
{
    public $question;
    public $questionType;
    private $pdo;

    public function __construct()
    {
        $servername = "localhost";
        $dbname = "pdo_crud";
        $username = "root";
        $password = "";
        try{
            $this->pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch (PDOException $e){
            echo $e->getMessage();
        }

    }

    public function prepareData($data = "")
    {
        if (array_key_exists("question", $data)) {
            $this->question = $data['question'];
        }
        if (array_key_exists("questionFor", $data)) {
            $this->questionType = $data['questionFor'];
        }
    }

    public function insertQuestion()
    {
        $sql = "INSERT INTO `te_question_bank`(`question`, `question_type`) VALUES ( :questionText, :questionTypeNumber)";
        $stmt = $this->pdo->prepare($sql);

        $stmt->bindParam(':questionText', $this->question);
        $stmt->bindParam(':questionTypeNumber', $this->questionType);

        $result = $stmt->execute();

        if ($result){
            Message::Message("
                <div class='alert alert-success'>
                  <strong>Stored!</strong> Data has been inserted successfully.
                </div>");
            header('Location:index.php');
        }else{
            Message::Message("
                <div class='alert alert-warning'>
                    Something went wrong.
                </div>");
            header('Location:index.php');
        }
    }

    public function viewAllQuestion()
    {
        $allData = [];
        $sql = "SELECT `id`, `question`, `question_type` FROM `te_question_bank`";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
            $allData[] = $row;
        }
        return $allData;
    }

    public function deleteQuestion($quesId)
    {
        $sql = "DELETE FROM `te_question_bank` WHERE `id` = :deletionID";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindParam(':deletionID', $quesId);
        $result = $stmt->execute();

        if ($result){
            Message::Message("
                <div class='alert alert-danger'>
                  <strong>Deleted!</strong> Data has been deleted successfully.
                </div>");
            header('Location:index.php');

        }else{
            Message::Message("
                <div class='alert alert-warning'>
                    Something went wrong.
                </div>");
            header('Location:index.php');
        }
    }
}