-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2017 at 10:15 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pdo_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `te_question_bank`
--

CREATE TABLE IF NOT EXISTS `te_question_bank` (
`id` int(11) NOT NULL,
  `question` varchar(255) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `te_question_bank`
--

INSERT INTO `te_question_bank` (`id`, `question`, `question_type`) VALUES
(1, 'Does the instructor come to the class in time?', 1),
(2, 'Does the instructor encourage reading reference material?', 1),
(3, 'Does the instructor explain the ideas/topics clearly', 1),
(4, 'Does the instructor give tutorial?', 1),
(5, 'Does the instructor mark the exam papers fairly?', 1),
(6, 'Does the instructor relate theory to practice?', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `te_question_bank`
--
ALTER TABLE `te_question_bank`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `te_question_bank`
--
ALTER TABLE `te_question_bank`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
